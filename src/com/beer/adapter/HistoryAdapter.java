package com.beer.adapter;

import java.util.ArrayList;

import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;
import com.deftsoft.beer.GlobalData;
import com.deftsoft.beer.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class HistoryAdapter extends ArrayAdapter<RecordTbl02> {

	public ArrayList<RecordTbl02> arrData;
	int row_id;
	Activity parentActivity;

	public HistoryAdapter(Context context, int _row_id, ArrayList<RecordTbl02> _arrData) {
		super(context, _row_id, _arrData);
		// TODO Auto-generated constructor stub
		parentActivity = (Activity) context;
		row_id = _row_id;
		arrData = _arrData;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        LayoutInflater inflater = parentActivity.getLayoutInflater();
        row = inflater.inflate(row_id, parent, false);
        
        TextView txtCLNO03 = (TextView) row.findViewById(R.id.clno03_textView);
        TextView txtCNNO03 = (TextView) row.findViewById(R.id.cnno03_textView);
        TextView txtSTSO03 = (TextView) row.findViewById(R.id.stso03_textView);
        TextView txtCUSN03 = (TextView) row.findViewById(R.id.cusn03_textView);
        
        RecordTbl02 record2 = arrData.get(position);
        
        String strCLNO03 = record2.mapField.get("CLNO03");// if (strCLNO03 == null) strCLNO03 = "null";
        String strCNNO03 = record2.mapField.get("CNNO03");// if (strCNNO03 == null) strCNNO03 = "null";
        String strSTSO03 = record2.mapField.get("STSO03");// if (strSTSO03 == null) strSTSO03 = "null";
        
        txtCLNO03.setText("CLNO03: " + strCLNO03);
        txtCNNO03.setText("CNNO03: " + strCNNO03);
        txtSTSO03.setText("STSO03: " + strSTSO03);
        
        RecordTbl01 record1 = GlobalData.getRecordTbl01FromBarCode("BR " + strCLNO03 + " " + strCNNO03);
        if (record1 != null) {
            String strCUSN03 = record1.mapField.get("CUSN03"); if (strCUSN03 == null) strCUSN03 = "null";
            txtCUSN03.setText("CUSN03: " + strCUSN03);
        } else {
            txtCUSN03.setText("CUSN03: null");
        }
        
//        imgAdIcon.setImageUrl("http://www.edumobile.org/iphone/wp-content/uploads/2013/06/Screenshot_41.png");
        return row;
	}
}