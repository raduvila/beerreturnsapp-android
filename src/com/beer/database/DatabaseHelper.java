package com.beer.database;

import java.util.ArrayList;

import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String LOG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Database.db";
    
    private static final String TABLE_TABLE1 = "table1";
    private static final String TABLE_TABLE2 = "table2";
    
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		String sqlCreateTable1 = "CREATE TABLE " + TABLE_TABLE1 + "(" +
				"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"CONO03 TEXT, " +
				"CLPX03 TEXT, " +
				"CLNO03 TEXT, " +
				"CNNO03 TEXT, " +
				"CUSN03 TEXT, " +
				"DSEQ03 TEXT, " +
				"FCOD03 TEXT, " +
				"STAT03 TEXT, " +
				"CDTY03 TEXT, " +
				"CTIM03 TEXT, " +
				"USID03 TEXT, " +
				"PNUM03 TEXT, " +
				"CSKN03 TEXT, " +
				"RACK03 TEXT, " +
				"FRCK03 TEXT, " +
				"CBBD03 TEXT, " +
				"FULL03 TEXT, " +
				"LDTE03 TEXT, " +
				"LTIM03 TEXT, " +
				"LBSN03 TEXT, " +
				"UDTE03 TEXT, " +
				"DDTE03 TEXT, " +
				"BDTE03 TEXT, " +
				"ADTE03 TEXT, " +
				"DIPD03 TEXT, " +
				"DIPT03 TEXT, " +
				"RFUS03 TEXT, " +
				"VDTE03 TEXT, " +
				"FJRN03 TEXT, " +
				"FDIP03 TEXT, " +
				"DIPV03 TEXT, " +
				"SBVT03 TEXT, " +
				"SBSN03 TEXT, " +
				"CREV03 TEXT, " +
				"DCVL03 TEXT, " +
				"DCTX03 TEXT, " +
				"DCRD03 TEXT, " +
				"SCRD03 TEXT, " +
				"SCCV03 TEXT, " +
				"LTFL03 TEXT, " +
				"LTOV03 TEXT, " +
				"CDFL03 TEXT, " +
				"AFLG03 TEXT, " +
				"VIST03 TEXT, " +
				"GLAB03 TEXT, " +
				"COMP03 TEXT, " +
				"ALTA03 TEXT, " +
				"TXLC03 TEXT, " +
				"TXFC03 TEXT, " +
				"ENUM03 TEXT, " +
				"VFCD03 TEXT, " +
				"LOCD03 TEXT, " +
				"MANA03 TEXT, " +
				"LBOK03 TEXT, " +
				"NOCA03 TEXT, " +
				"NCUL03 TEXT, " +
				"ABVL03 TEXT, " +
				"SPAR03 TEXT" + ")";
		
		String sqlCreateTable2 = "CREATE TABLE " + TABLE_TABLE2 + "(" +
				"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "CLNO03 TEXT, " +
                "CNNO03 TEXT, " +
                "STSO03 TEXT, " +
                "DIPV03 TEXT, " +
                "CTTO03 TEXT, " +
                "CFCO03 TEXT, " +
                "TANK03 TEXT, " +
                "MODO03 TEXT, " +
                "SYNC03 TEXT" + ")";
		db.execSQL(sqlCreateTable1);
		db.execSQL(sqlCreateTable2);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TABLE1);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TABLE2);
        
         // create new tables
        onCreate(db);
	}
	
	public long addRecord1(RecordTbl01 record) {
		SQLiteDatabase db = this.getWritableDatabase();
		
	    ContentValues values = new ContentValues();
	    
	    for (String strKey : record.arrKey) {
	    	values.put(strKey, record.mapField.get(strKey));
	    }
	    // insert row
	    long record_id = db.insert(TABLE_TABLE1, null, values);
	    
	    db.close();
	 
	    return record_id;
	}
	
	public long addRecord2(RecordTbl02 record) {
		SQLiteDatabase db = this.getWritableDatabase();
		 
	    ContentValues values = new ContentValues();
	    
	    for (String strKey : record.arrKey) {
	    	values.put(strKey, record.mapField.get(strKey));
	    }
	    // insert row
	    long record_id = db.insert(TABLE_TABLE2, null, values);
	    
	    db.close();
	    return record_id;
	}
	
	public void deleteAllRecord1() {
		SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TABLE1, null, null);
        db.close();
	}
	
	public void deleteAllRecord2() {
		SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TABLE2, null, null);
        db.close();
	}
	
	public ArrayList<RecordTbl01> getAllRecord1() {
		ArrayList<RecordTbl01> arrRecord = new ArrayList<RecordTbl01>();
		String selectQuery = "SELECT  * FROM " + TABLE_TABLE1;
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        int nCount = cursor.getColumnCount();
        if (cursor.moveToFirst()) {
            do {
            	RecordTbl01 record = new RecordTbl01();
            	record.id = cursor.getInt(0);
            	for (int i = 1; i < nCount; i++) {
            		String colName = cursor.getColumnName(i);
            		String colValue = cursor.getString(i);
            		record.arrKey.add(colName); if (colValue == null) colValue = "";
            		record.mapField.put(colName, colValue);
//            		Log.e("table01", colName + ", " + colValue);
            	}
                arrRecord.add(record);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
		return arrRecord;
	}
	
	public ArrayList<RecordTbl02> getAllRecord2() {
		ArrayList<RecordTbl02> arrRecord = new ArrayList<RecordTbl02>();
		String selectQuery = "SELECT  * FROM " + TABLE_TABLE2 + " ORDER BY MODO03 DESC";
		
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        int nCount = cursor.getColumnCount();
        if (cursor.moveToFirst()) {
            do {
            	RecordTbl02 record = new RecordTbl02();
            	record.id = cursor.getInt(0);
//            	Log.e("table02 record id", Integer.toString(record.id));
            	for (int i = 1; i < nCount; i++) {
            		String colName = cursor.getColumnName(i);
            		String colValue = cursor.getString(i); if (colValue == null) colValue = "";
            		record.arrKey.add(colName);
            		record.mapField.put(colName, colValue);
            	}
                arrRecord.add(record);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
		return arrRecord;
	}
	
	public void updateRecord1(long id, ContentValues updateValues) {
		
		SQLiteDatabase db = this.getWritableDatabase();
	  	
	    db.update(TABLE_TABLE1, updateValues, "id = ? ", new String[] { Long.toString(id) } );
	}
	
	public void updateRecord2(long id, ContentValues updateValues) {
		
		SQLiteDatabase db = this.getWritableDatabase();
	  	
	    db.update(TABLE_TABLE2, updateValues, "id = ? ", new String[] { Long.toString(id) } );
	}
}
