package com.deftsoft.beer;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.beer.database.DatabaseHelper;
import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;
import com.deftsoft.beer.R;
import com.han.network.BeerAsyncTask;
import com.han.utility.NetworkUtility;

public class Synchronization extends Activity implements OnClickListener {
	
	Button btnCancel;
	ListView lstSynchron;
	ArrayList<String> arrSync;
	ArrayAdapter<String> adapter;
	
	public int nCountDownloaded;
	
//	boolean isSuccessReadTable1FromServer;
//	boolean isSuccessReadTable2FromServer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_synchnronization);
		
		initWiget();
		initValue();
		initEvent();
	}

	private void initWiget() {
		// TODO Auto-generated method stub

		btnCancel = (Button) findViewById(R.id.synch_cancel_button);
		lstSynchron = (ListView)findViewById(R.id.synch_listView);
	}
	
	private void initValue() {
		
//		isSuccessReadTable1FromServer = false;
//		isSuccessReadTable1FromServer = false;
		if (NetworkUtility.isOnline(this)) {

			nCountDownloaded = 0;
			
			arrSync = new ArrayList<String>();
			arrSync.add("Downloading Returns");
			
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, arrSync);
			
			lstSynchron.setAdapter(adapter);
			
			DatabaseHelper dbHelper = new DatabaseHelper(this);
			dbHelper.deleteAllRecord1();
			GlobalData.arrRecordTbl01 = dbHelper.getAllRecord1();
			
			Log.e("table01 recoud count", Integer.toString(GlobalData.arrRecordTbl01.size()));
		
			new BeerAsyncTask(this).execute(BeerAsyncTask.ACTION_SYNC, "", "");	
		}
	}
	
	private void initEvent() {

		lstSynchron.setOnItemClickListener(new OnItemClickListener() {  
			@Override
		    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int itemPosition = position;
				String  itemValue  = (String) lstSynchron.getItemAtPosition(position);
			}
		});
		btnCancel.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.equals(btnCancel)) {
			//CommonMethods.showToast(Synchronization.this, "Cancel");
			finish();
		}
	}
	
	public void successReadAllTable1() {

		Log.e("function in", "successReadAllTable");
//		arrSync.add("All Returns downloaded successfully");
		addLog("All Returns downloaded successfully");
		DatabaseHelper dbHelper = new DatabaseHelper(this);
//		dbHelper.deleteAllRecord1();
		
//		for (RecordTbl01 record : GlobalData.arrRecordTbl01) {
//			dbHelper.addRecord1(record);
//		}
		
		GlobalData.arrRecordTbl01 = dbHelper.getAllRecord1();
	}
	
	public void successReadTable1() {
//		if (!isSuccessReadTable1FromServer || !isSuccessReadTable2FromServer) return;
		DatabaseHelper dbHelper = new DatabaseHelper(this);
//		dbHelper.deleteAllRecord1();
		
		for (RecordTbl01 record : GlobalData.arrReturnRecordTbl01) {
			nCountDownloaded++;
			arrSync.add(Integer.toString(nCountDownloaded) + " downloaded");
			adapter.notifyDataSetChanged();
			dbHelper.addRecord1(record);
		}
//		GlobalData.arrRecordTbl01 = dbHelper.getAllRecord1();
	}
	
	public void addLog(String strSync) {
		arrSync.add(strSync);
		adapter.notifyDataSetChanged();
	}
	
	public void suceessUploadTable2() {
		btnCancel.setText("Done");
	}
}
