package com.deftsoft.beer;

import java.util.ArrayList;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.beer.database.DatabaseHelper;
import com.deftsoft.beer.R;
import com.han.utility.TimeUtility;

public class LeakReport extends Activity implements OnClickListener {
	
	Button btnBack, btnHome, btnComplete;
	Spinner spnContainerType, spnFailureCode;
	TextView Details;
	EditText LeakReport_Value;
	
	DatabaseHelper dbHelper;
	public static long cur_record2_id;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leak_report);

		initWidget();
		initValue();
		initEvent();
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.leak_back);
		btnHome = (Button) findViewById(R.id.leak_home);
		btnComplete = (Button) findViewById(R.id.leak_complete);
		spnContainerType = (Spinner) findViewById(R.id.leak_container_type_spinner);
		spnFailureCode = (Spinner) findViewById(R.id.leak_container_failure_code_spinner);
	}
	
	private void initValue() {

		dbHelper = new DatabaseHelper(this);

		ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, GlobalData.arrContainerType);
		typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnContainerType.setAdapter(typeAdapter);

		ArrayAdapter<String> failureAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, GlobalData.arrFailureCode);
		failureAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnFailureCode.setAdapter(failureAdapter);
	}

	private void initEvent() {
		btnBack.setOnClickListener(this);
		btnHome.setOnClickListener(this);
		btnComplete.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.equals(btnBack)) {
			//CommonMethods.showToast(LeakReport.this, "Back");
			finish();
		}
		if (v.equals(btnHome)) {
			//CommonMethods.showToast(LeakReport.this, "Home");
			setResult(GlobalData.RESULT_CODE_HOME);
			finish();
		}
		if (v.equals(btnComplete)) {
			
			ContentValues updateValues = new ContentValues();
			updateValues.put("CTTO03", GlobalData.getContainerTypeValue(spnContainerType.getSelectedItemPosition()));
			updateValues.put("CFCO03", GlobalData.getFailureCodeValue(spnFailureCode.getSelectedItemPosition()));
			updateValues.put("STSO03", "COMPLETE");
			updateValues.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));

			dbHelper.updateRecord2(cur_record2_id, updateValues);
			GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
			
//			CommonMethods.showToast(ViewData.this, "COMPLETE");
			setResult(GlobalData.RESULT_CODE_BEER_RETURN);
			finish();
		}
	}
}
