package com.deftsoft.beer;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.beer.adapter.HistoryAdapter;
import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;

public class History extends Activity implements OnClickListener {
	
	private final int REQUEST_CODE_VIEW_DATA = 1001;

	Button btnBack, btnHome, btnNext, btnPrevious, btnAdd;
	int count = 1;
	ListView lstHistory;
	HistoryAdapter historyAdapter;
	
	ArrayList<String[]> Tag_Details;
	ArrayList<String[]> Parser_array;
	ArrayList<String[]> Final_List;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);

		initWidget();
		initValue();
		initEvent();
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.history_back_button);
		btnNext = (Button) findViewById(R.id.history_next_button);
		btnPrevious = (Button) findViewById(R.id.history_previous_button);
		btnAdd = (Button) findViewById(R.id.history_add_button);

		btnHome = (Button) findViewById(R.id.history_home_button);

		lstHistory = (ListView) findViewById(R.id.history_list_view);
	}
	
	private void initValue() {
		historyAdapter = new HistoryAdapter(this, R.layout.row_history, GlobalData.arrRecordTbl02);
		lstHistory.setAdapter(historyAdapter);
	}

	private void initEvent() {
		btnBack.setOnClickListener(this);
		btnHome.setOnClickListener(this);
		btnNext.setOnClickListener(this);
		btnPrevious.setOnClickListener(this);
		btnAdd.setOnClickListener(this);
		
		lstHistory.setOnItemClickListener(new OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	        	
	        	RecordTbl02 record2 = historyAdapter.arrData.get(position);
	        	String strCLNO03 = record2.mapField.get("CLNO03");
	            String strCNNO03 = record2.mapField.get("CNNO03");
	            RecordTbl01 record1 = GlobalData.getRecordTbl01FromBarCode("BR " + strCLNO03 + " " + strCNNO03);
	            
	            ViewData.record1 = record1;
	            ViewData.cur_record2_id = record2.id;
	            
	            History.this.startActivityForResult(new Intent(History.this, ViewData.class), REQUEST_CODE_VIEW_DATA);
	        }
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.equals(btnBack)) {
			// CommonMethods.showToast(History.this, "Back");
			finish();
		}
		if (v.equals(btnHome)) {
			// CommonMethods.showToast(History.this, "Home");
			finish();
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Handle the logic for the requestCode, resultCode and data returned...
		historyAdapter.arrData = GlobalData.arrRecordTbl02;
		historyAdapter.notifyDataSetChanged();
		
		if (requestCode == REQUEST_CODE_VIEW_DATA) {
			if (resultCode == GlobalData.RESULT_CODE_HOME) {
				finish();
			}
		}
	}
}
