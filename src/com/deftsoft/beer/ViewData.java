package com.deftsoft.beer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.beer.database.DatabaseHelper;
import com.beer.model.RecordTbl01;
import com.deftsoft.beer.R;
import com.han.utility.TimeUtility;

public class ViewData extends Activity implements OnClickListener {
	private final int REQUEST_CODE_DIP = 1002;
	
	Button btnBack, btnHome, btnComplete, btnDip, btnReject, btnSample;
	TextView txtDetails;
	
	public static long cur_record2_id;
	public static RecordTbl01 record1;
	DatabaseHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_data);

		initWidget();
		initValue();
		initEvent();
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.view_data_back_button);
		btnHome = (Button) findViewById(R.id.view_data_home_button);
		btnComplete = (Button) findViewById(R.id.view_data_complete_button);
		btnDip = (Button) findViewById(R.id.view_data_dip_button);
		btnReject = (Button) findViewById(R.id.view_data_reject_button);
		btnSample = (Button) findViewById(R.id.view_data_sample_button);
		txtDetails = (TextView) findViewById(R.id.view_data_details_textView);
		txtDetails.setText("Product code    " + record1.mapField.get("PNUM03") + "\n"
				+ "Refusal code    " + record1.mapField.get("RFUS03") + "\n" + "Other fiels    ????");
	}
	
	private void initValue() {
		dbHelper = new DatabaseHelper(this);
	}

	private void initEvent() {
		btnBack.setOnClickListener(this);
		btnHome.setOnClickListener(this);
		btnComplete.setOnClickListener(this);
		btnDip.setOnClickListener(this);
		btnReject.setOnClickListener(this);
		btnSample.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.equals(btnBack)) {
			finish();
		}
		if (v.equals(btnHome)) {
			//CommonMethods.showToast(ViewData.this, "Home");
			this.setResult(GlobalData.RESULT_CODE_HOME);
			finish();
		}

		if (v.equals(btnComplete)) {
			ContentValues updateValues = new ContentValues();
			updateValues.put("STSO03", "COMPLETE");
			updateValues.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));
			
			dbHelper.updateRecord2(cur_record2_id, updateValues);
			GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
			
//			CommonMethods.showToast(ViewData.this, "COMPLETE");
			setResult(GlobalData.RESULT_CODE_HOME);
			finish();
		}
		if (v.equals(btnDip)) {
//			ContentValues updateValues = new ContentValues();
//			updateValues.put("STSO03", "DIP");
//			updateValues.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));
//			
//			dbHelper.updateRecord2(cur_record2_id, updateValues);
//			GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
			
//			CommonMethods.showToast(ViewData.this, "DIP");
			
			Dip.cur_record2_id = cur_record2_id;
			startActivityForResult(new Intent(this, Dip.class), REQUEST_CODE_DIP);
		}
		if (v.equals(btnReject)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

		    builder.setTitle("Confirm");
		    builder.setMessage("Are you sure?");

		    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

		        public void onClick(DialogInterface dialog, int which) {
		            // Do nothing but close the dialog
					ContentValues updateValues = new ContentValues();
					updateValues.put("STSO03", "REJECT");
					updateValues.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));
					
					dbHelper.updateRecord2(cur_record2_id, updateValues);
					GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
					
//					CommonMethods.showToast(ViewData.this, "REJECT");
					finish();
		            dialog.dismiss();
		        }
		    });

		    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		            // Do nothing
		            dialog.dismiss();
		        }
		    });

		    AlertDialog alert = builder.create();
		    alert.show();
		}
		if (v.equals(btnSample)) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

		    builder.setTitle("Confirm");
		    builder.setMessage("Are you sure?");

		    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

		        public void onClick(DialogInterface dialog, int which) {
		            // Do nothing but close the dialog
					ContentValues updateValues = new ContentValues();
					updateValues.put("STSO03", "SAMPLE");
					updateValues.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));
					
					dbHelper.updateRecord2(cur_record2_id, updateValues);
					GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
					
//					CommonMethods.showToast(ViewData.this, "SAMPLE");
					finish();
		            dialog.dismiss();
		        }
		    });

		    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		            // Do nothing
		            dialog.dismiss();
		        }
		    });
		    
		    AlertDialog alert = builder.create();
		    alert.show();
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Handle the logic for the requestCode, resultCode and data returned...
		if (requestCode == REQUEST_CODE_DIP) {
			if (resultCode == GlobalData.RESULT_CODE_HOME) {
				setResult(GlobalData.RESULT_CODE_HOME);
				finish();
			}
			if (resultCode == GlobalData.RESULT_CODE_BEER_RETURN) {
				setResult(GlobalData.RESULT_CODE_BEER_RETURN);
				finish();
			}
		}
	}
}
