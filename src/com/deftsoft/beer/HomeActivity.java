package com.deftsoft.beer;

import com.beer.database.DatabaseHelper;
import com.han.utility.TimeUtility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends Activity {
	Button btnReturn, btnHistory, btnSync, btnDestruction;
	public TextView txtCurrenTankDesc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		
		btnReturn = (Button) findViewById(R.id.return_button);
		btnHistory = (Button) findViewById(R.id.history_button);
		btnSync = (Button) findViewById(R.id.sync_button);
		btnDestruction = (Button) findViewById(R.id.destruction_button);
		
		txtCurrenTankDesc = (TextView) findViewById(R.id.current_tank_desc_textView);
	}
	
	private void initValue() {
		DatabaseHelper dbHelper = new DatabaseHelper(this);

		GlobalData.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		GlobalData.preferencesEditor = GlobalData.sharedPreferences.edit();
		
		GlobalData.arrRecordTbl01 = dbHelper.getAllRecord1();
		GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
		
		GlobalData.currentTankID = GlobalData.sharedPreferences.getString(GlobalData.PREFERENCE_KEY_STRING_CURRENT_TANK_ID, 
				TimeUtility.getCurrentTimeAsFormat("yyyyMMddHHmmss"));
		GlobalData.currentServerUrl = GlobalData.sharedPreferences.getString(GlobalData.PREFERENCE_KEY_SERVER_URL, GlobalData.DEFAULT_SERVER_URL);
		GlobalData.currentUserName = GlobalData.sharedPreferences.getString(GlobalData.PREFERENCE_KEY_USER_NAME, GlobalData.DEFAULT_USER_NAME);
		GlobalData.currentPassword = GlobalData.sharedPreferences.getString(GlobalData.PREFERENCE_KEY_PASSWORD, GlobalData.DEFAULT_PASSWORD);
		

		
//		Log.e("test", TimeUtility.getDateFromString("", GlobalData.STRING_TIME_FORMAT_MODO03).toString());
	}
	
	private void initEvent() {
		btnReturn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				Intent intent = new Intent(HomeActivity.this, BeerReturn.class);
				startActivity(intent);
//				finish();
			}
		});
		btnHistory.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				Intent intent = new Intent(HomeActivity.this, History.class);
				startActivity(intent);
//				finish();
			}
		});
		btnSync.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				Intent intent = new Intent(HomeActivity.this, Synchronization.class);
				startActivity(intent);
//				finish();
			}
		});
		btnDestruction.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
//				CommonMethods.showToast(HomeActivity.this, "Destruction");
//				new BeerAsyncTask(HomeActivity.this).execute(BeerAsyncTask.ACTION_READ_TABLE02, "", "");
				AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);

			    builder.setTitle("Confirm");
			    builder.setMessage("Are you sure?");

			    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing but close the dialog
						GlobalData.currentTankID = TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_TANK03);
						
						GlobalData.preferencesEditor.putString(GlobalData.PREFERENCE_KEY_STRING_CURRENT_TANK_ID, GlobalData.currentTankID);
						GlobalData.preferencesEditor.commit();
						
						String str = GlobalData.getCurrentTankDutyLevel();
						txtCurrenTankDesc.setText("Current Duty Tank Level: " + str + " Gallons");
						
			            dialog.dismiss();
			        }
			    });

			    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

			        @Override
			        public void onClick(DialogInterface dialog, int which) {
			            // Do nothing
			            dialog.dismiss();
			        }
			    });

			    AlertDialog alert = builder.create();
			    alert.show();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch(item.getItemId())
		{
			case R.id.menuitem_settings:
				startActivity(new Intent(this, Setting.class));
				return true;
//			case R.id.menuitem_about:
				
//				return true;
			default:
				return false;
		}
	}
	
	protected void onResume() {
		super.onResume();
		
		String str = GlobalData.getCurrentTankDutyLevel();
		txtCurrenTankDesc.setText("Current Duty Tank Level: " + str + " Gallons");
	}
}
