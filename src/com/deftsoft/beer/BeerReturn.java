package com.deftsoft.beer;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.beer.database.DatabaseHelper;
import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;
import com.deftsoft.beer.R;
import com.han.utility.DialogUtility;
import com.han.utility.TimeUtility;

public class BeerReturn extends Activity implements OnClickListener {
	
	private final int REQUEST_CODE_VIEW_DATA = 1001;
	
	Button btnBack, btnHome, btnViewData;
	EditText edtBarCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_beer_return);

		initWidget();
		initValue();
		initEvent();
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.beer_return_back_button);
		btnHome = (Button) findViewById(R.id.beer_return_home_button);
		btnViewData = (Button) findViewById(R.id.beer_return_view_data_button);
		
		edtBarCode = (EditText) findViewById(R.id.bar_code_editText);
	}
	
	private void initValue() {
//		for (int i = 0; i < GlobalData.arrRecordTbl01.size(); i++) {
//			RecordTbl01 record = GlobalData.arrRecordTbl01.get(i);
//			String CNNO03 = record.mapField.get("CNNO03");
//			
//			Log.e("tbl01 CNNO03 " + Integer.toString(i), CNNO03);
//		}
//		
		for (int i = 0; i < GlobalData.arrRecordTbl02.size(); i++) {
			RecordTbl02 record = GlobalData.arrRecordTbl02.get(i);
			String CLNO03 = record.mapField.get("CLNO03");
			
			Log.e("tbl02 CLNO03 " + Integer.toString(i), CLNO03);
		}
	}

	private void initEvent() {
		btnBack.setOnClickListener(this);
		btnHome.setOnClickListener(this);
		btnViewData.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.equals(btnBack)) {
			//CommonMethods.showToast(BeerReturn.this, "Back");
			//finish();

			finish();
		}
		if (v.equals(btnHome)) {
			//CommonMethods.showToast(BeerReturn.this, "Home");
//			Intent in = new Intent(BeerReturn.this, HomeActivity.class);
//			startActivity(in);
			finish();
		}
		if (v.equals(btnViewData)) {
			//CommonMethods.showToast(BeerReturn.this, "View_Data");
			
			String strBarCode = edtBarCode.getText().toString();
			if (strBarCode.equals("") || strBarCode == null) {
				DialogUtility.show(BeerReturn.this, "Please Input BarCode");
				return;
			}
			
			if (!validCheckBarCode()) {
				DialogUtility.showGeneralAlert(this, "Error", "Invalid Return Number");
				return;
			}

			RecordTbl01 record1 = GlobalData.getRecordTbl01FromBarCode(strBarCode);
			
			if (record1 == null) {
				DialogUtility.show(BeerReturn.this, "This code is not exist on table01");
				return;
			}
			
			RecordTbl02 record2 = GlobalData.getNewRecordTbl02FromBarCode(strBarCode);
			
			DatabaseHelper dbHelper = new DatabaseHelper(this);
			
			long new_id = dbHelper.addRecord2(record2);
			GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
			Log.e("table02 new id", Long.toString(new_id));
			
			ViewData.record1 = record1;
			ViewData.cur_record2_id = new_id;
			
			Intent in = new Intent(BeerReturn.this, ViewData.class);
			this.startActivityForResult(in, REQUEST_CODE_VIEW_DATA);
		}
	}
	
	private boolean validCheckBarCode() {
		String strBarCode = edtBarCode.getText().toString();
		
		try {
			String str[] = strBarCode.split(" ");
			Log.e("str length", Integer.toString(str.length));
			
			String strCLNO03 = str[1];
			String strCNNO03 = str[2];

			int CLNO03 = Integer.parseInt(strCLNO03);
			int CNNO03 = Integer.parseInt(strCNNO03);
			return true;
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Handle the logic for the requestCode, resultCode and data returned...
		if (requestCode == REQUEST_CODE_VIEW_DATA) {
			if (resultCode == GlobalData.RESULT_CODE_HOME) {
				finish();
			}
			if (resultCode == GlobalData.RESULT_CODE_BEER_RETURN) {
				edtBarCode.setText("");
			}
		}
	}
}
