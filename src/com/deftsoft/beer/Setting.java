package com.deftsoft.beer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.deftsoft.beer.R;
import com.han.utility.DialogUtility;

public class Setting extends Activity implements OnClickListener {
	Button btnSave, btnCancel;
	EditText edtUrl, edtUserName, edtPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		initWidget();
		initValue();
		initEvent();
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		btnSave = (Button) findViewById(R.id.setting_save_button);
		btnCancel = (Button) findViewById(R.id.setting_cancel_button);
		edtUrl = (EditText) findViewById(R.id.setting_url_editText);
		edtUserName = (EditText) findViewById(R.id.setting_username_editText);
		edtPassword = (EditText) findViewById(R.id.setting_password_editText);
	}
	
	private void initValue() {
		edtUrl.setText(GlobalData.currentServerUrl);
		edtUserName.setText(GlobalData.currentUserName);
		edtPassword.setText(GlobalData.currentPassword);
	}

	private void initEvent() {
		btnSave.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.equals(btnSave)) {
			//CommonMethods.showToast(Setting.this, "Save");
			saveData();
		}
		if (v.equals(btnCancel)) {
			finish();
		}
	}

	private void saveData() {

		String strUrl = edtUrl.getText().toString();
		String strUserName = edtUserName.getText().toString();
		String strPassword = edtPassword.getText().toString();

		if (!strUrl.equals("") && !strUserName.equals("") && !strPassword.equals("")) {
			
			DialogUtility.show(Setting.this, "Saving data ");
			
			GlobalData.preferencesEditor.putString(GlobalData.PREFERENCE_KEY_SERVER_URL, strUrl);
			GlobalData.preferencesEditor.putString(GlobalData.PREFERENCE_KEY_USER_NAME, strUserName);
			GlobalData.preferencesEditor.putString(GlobalData.PREFERENCE_KEY_PASSWORD, strPassword);
			GlobalData.preferencesEditor.commit();

			GlobalData.currentServerUrl = strUrl;
			GlobalData.currentUserName = strUserName;
			GlobalData.currentPassword = strPassword;
			
			finish();
		} else {
			DialogUtility.show(Setting.this, "Fill required fields ");
		}
	}
}
