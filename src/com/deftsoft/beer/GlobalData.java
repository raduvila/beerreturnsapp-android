package com.deftsoft.beer;

import java.util.ArrayList;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;
import com.han.utility.TimeUtility;

public class GlobalData {
	
	public static final String STRING_TIME_FORMAT_MODO03 = "yyyy-MM-dd-HH.mm.ss.00000"; 
	public static final String STRING_TIME_FORMAT_SYNC03 = "yyyy-MM-dd-HH.mm.ss.00000";
	public static final String STRING_TIME_FORMAT_TANK03 = "yyyyMMddHHmmss";
	
	public static final String PREFERENCE_KEY_STRING_CURRENT_TANK_ID = "currentTankID";
	public static final String PREFERENCE_KEY_SERVER_URL = "server_url";
	public static final String PREFERENCE_KEY_USER_NAME = "user_name";
	public static final String PREFERENCE_KEY_PASSWORD = "password";
	
	public static final String DEFAULT_SERVER_URL = "http://82.69.20.215:6400/data/test";
	public static final String DEFAULT_USER_NAME = "DSTEST";
	public static final String DEFAULT_PASSWORD = "DSTEST";
	
	public static final int RESULT_CODE_HOME = 100;
	public static final int RESULT_CODE_BEER_RETURN = 101;
	
	public static String[] arrContainerType = {"Stainless", "Aluminium", "Rubber"};
	public static String[] arrFailureCode = {"Miss Shive", "Leaking Shive", "Leaking Keystone", "Damaged Container"};
	
	public static String currentTankID;
	public static String currentServerUrl;
	public static String currentUserName;
	public static String currentPassword;

	public static ArrayList<RecordTbl01> arrRecordTbl01 = new ArrayList<RecordTbl01>();
	public static ArrayList<RecordTbl02> arrRecordTbl02 = new ArrayList<RecordTbl02>();
	
	public static ArrayList<RecordTbl01> arrReturnRecordTbl01 = new ArrayList<RecordTbl01>();

	public static SharedPreferences sharedPreferences;
	public static Editor preferencesEditor;
	
	public static int COUNT_RECORD_TABLE01 = 1;
	public static int COUNT_RECORD_READ_TABLE01_ONCE = 50;
	
	public static String getCurrentTankDutyLevel() {
		
		float sum = 0.0f;
		for (int i = 0; i < arrRecordTbl02.size(); i++) {
			RecordTbl02 record = arrRecordTbl02.get(i);
			
			String strTANK03 = record.mapField.get("TANK03");
			if (strTANK03 == null) continue;
			if (strTANK03.equals(GlobalData.currentTankID)) {
				String strDIPV03 = record.mapField.get("DIPV03");
				if (strDIPV03 != null) {
					float l = Float.parseFloat(strDIPV03);
					sum += l;
				}
			}
//			Log.e(Float.toString(l), Float.toString(sum));
		}
		
		return String.format("%.1f", sum);
	}
	
	public static RecordTbl01 getRecordTbl01FromBarCode(String strBarCode) {
		
		String str[] = strBarCode.split(" ");
		String strCLNO03 = str[1];
		String strCNNO03 = str[2];
		int nCLNO03 = Integer.parseInt(strCLNO03);
		int nCNNO03 = Integer.parseInt(strCNNO03);
		
		Log.e("CLNO03, CNNO03", strCLNO03 + ", " + strCNNO03);
		
		for (int i = 0; i < arrRecordTbl01.size(); i++) {
			RecordTbl01 record = arrRecordTbl01.get(i);
			String tmpCLNO03 = record.mapField.get("CLNO03");
			String tmpCNNO03 = record.mapField.get("CNNO03");
			int CLNO03 = Integer.parseInt(tmpCLNO03);
			int CNNO03 = Integer.parseInt(tmpCNNO03);
			
			if (nCLNO03 == CLNO03) {
				Log.e("CNNO03 value", Integer.toString(CNNO03));
				if (CNNO03 == nCNNO03) {
					return record;
				}
			}
		}
		
		return null;
	}
	
	public static RecordTbl02 getNewRecordTbl02FromBarCode(String strBarCode) {
		String strCLNO03 = strBarCode.substring(3, 10);
		String strCNNO03 = strBarCode.substring(11, 13);
		
		RecordTbl02 record2 = new RecordTbl02();
		
		record2.arrKey.add("CLNO03");
		record2.mapField.put("CLNO03", strCLNO03);
		record2.arrKey.add("CNNO03");
		record2.mapField.put("CNNO03", strCNNO03);
		record2.arrKey.add("STSO03");
		record2.mapField.put("STSO03", "SCAN");
		record2.arrKey.add("MODO03");
		record2.mapField.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));
		
		return record2;
	}
	
	public static RecordTbl02 getRecordTbl02FromBarCode(String strBarCode) {
		
		String strCLNO03 = strBarCode.substring(3, 10);
		String strCNNO03 = strBarCode.substring(11, 13);
		int nCLNO03 = Integer.parseInt(strCLNO03);
		int nCNNO03 = Integer.parseInt(strCNNO03);
		
		Log.e("CLNO03, CNNO03", strCLNO03 + ", " + strCNNO03);
		
		for (int i = 0; i < arrRecordTbl02.size(); i++) {
			RecordTbl02 record = arrRecordTbl02.get(i);
			String tmpCLNO03 = record.mapField.get("CLNO03");
			String tmpCNNO03 = record.mapField.get("CNNO03");
			
			//Log.e("CLNO03 value", tmpCLNO03);

			int CLNO03 = Integer.parseInt(tmpCLNO03);
			int CNNO03 = Integer.parseInt(tmpCNNO03);
			
			if (nCLNO03 == CLNO03) {
				Log.e("CNNO03 value", Integer.toString(CNNO03));
				if (CNNO03 == nCNNO03) {
					return record;
				}
			}
		}
		
		return null;
	}
	
	public static String getContainerTypeValue(int index) {
		String[] arrStr = {"K", "L", "R"};
		return arrStr[index];
	}
	
	public static String getFailureCodeValue(int index) {
		String[] arrStr = {"A", "B", "C", "D"};
		return arrStr[index];
	}
}
