package com.deftsoft.beer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.beer.database.DatabaseHelper;
import com.deftsoft.beer.R;
import com.han.utility.TimeUtility;

public class Dip extends Activity implements OnClickListener {
	
	private final int REQUEST_CODE_LEAK_REPORT = 1003;
	
	Button btnBack, btnHome, btnDuty, btnWaste, btnDestruction;
	TextView txtCurrenTankDesc;
	EditText edtDipValue;

	public static long cur_record2_id;
	DatabaseHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dip);
    
		initWidget();
		initValue();
		initEvent();
	}

	private void initWidget() {
		// TODO Auto-generated method stub
		btnBack = (Button) findViewById(R.id.dip_back_button);
		btnHome = (Button) findViewById(R.id.dip_home_button);
		btnDuty = (Button) findViewById(R.id.dip_duty_button);
		btnWaste = (Button) findViewById(R.id.dip_waste_button);
		btnDestruction = (Button) findViewById(R.id.dip_destruction_button);

		txtCurrenTankDesc = (TextView) findViewById(R.id.dip_current_tank_desc_textView);
		edtDipValue = (EditText) findViewById(R.id.dip_values_editText);
	}
	
	private void initValue() {
		dbHelper = new DatabaseHelper(this);
		
		String str = GlobalData.getCurrentTankDutyLevel();
		txtCurrenTankDesc.setText("Current Duty Tank Level: " + str + " Gallons");
	}

	private void initEvent() {
		btnBack.setOnClickListener(this);
		btnHome.setOnClickListener(this);
		btnDuty.setOnClickListener(this);
		btnWaste.setOnClickListener(this);
		btnDestruction.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.equals(btnBack)) {
			//CommonMethods.showToast(Dip.this, "Back");
			finish();
		}
		if (v.equals(btnHome)) {
			//CommonMethods.showToast(Dip.this, "Home");
			setResult(GlobalData.RESULT_CODE_HOME);
			finish();
		}
		if (v.equals(btnDuty)) {
//			CommonMethods.showToast(Dip.this, "Duty");
			ContentValues updateValues = new ContentValues();
			
			String strValue = edtDipValue.getText().toString();
			if (strValue == null || strValue.equals("")) {
				strValue = "0.0";
			}
			updateValues.put("DIPV03", strValue);
			updateValues.put("STSO03", "LEAK");
			updateValues.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));
			updateValues.put("TANK03", GlobalData.currentTankID);

			dbHelper.updateRecord2(cur_record2_id, updateValues);
			GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
			
			String str = GlobalData.getCurrentTankDutyLevel();
			txtCurrenTankDesc.setText("Current Duty Tank Level: " + str + " Gallons");
			
			LeakReport.cur_record2_id = cur_record2_id;
			startActivityForResult(new Intent(this, LeakReport.class), REQUEST_CODE_LEAK_REPORT);
		}
		if (v.equals(btnWaste)) {
//			CommonMethods.showToast(Dip.this, "Waste");
			ContentValues updateValues = new ContentValues();
			
			String strValue = edtDipValue.getText().toString();
			if (strValue == null || strValue.equals("")) {
				strValue = "0.0";
			}
			updateValues.put("DIPV03", strValue);
			updateValues.put("STSO03", "LEAK");
			updateValues.put("MODO03", TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_MODO03));
			updateValues.put("TANK03", "");

			dbHelper.updateRecord2(cur_record2_id, updateValues);
			GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
			
			String str = GlobalData.getCurrentTankDutyLevel();
			txtCurrenTankDesc.setText("Current Duty Tank Level: " + str + " Gallons");
			
			LeakReport.cur_record2_id = cur_record2_id;
			startActivityForResult(new Intent(this, LeakReport.class), REQUEST_CODE_LEAK_REPORT);
		}
		if (v.equals(btnDestruction)) {
//			CommonMethods.showToast(Dip.this, "Destruction");
			AlertDialog.Builder builder = new AlertDialog.Builder(Dip.this);

		    builder.setTitle("Confirm");
		    builder.setMessage("Are you sure?");

		    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

		        public void onClick(DialogInterface dialog, int which) {
		            // Do nothing but close the dialog
					GlobalData.currentTankID = TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_TANK03);
					
					GlobalData.preferencesEditor.putString(GlobalData.PREFERENCE_KEY_STRING_CURRENT_TANK_ID, GlobalData.currentTankID);
					GlobalData.preferencesEditor.commit();
					
		            dialog.dismiss();
		            
					String str = GlobalData.getCurrentTankDutyLevel();
					txtCurrenTankDesc.setText("Current Duty Tank Level: " + str + " Gallons");
					
					setResult(GlobalData.RESULT_CODE_HOME);
					finish();
		        }
		    });

		    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		            // Do nothing
		            dialog.dismiss();
		        }
		    });

		    AlertDialog alert = builder.create();
		    alert.show();			
		}
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Handle the logic for the requestCode, resultCode and data returned...
		if (requestCode == REQUEST_CODE_LEAK_REPORT) {
			if (resultCode == GlobalData.RESULT_CODE_HOME) {
				setResult(GlobalData.RESULT_CODE_HOME);
				finish();
			}
			if (resultCode == GlobalData.RESULT_CODE_BEER_RETURN) {
				setResult(GlobalData.RESULT_CODE_BEER_RETURN);
				finish();
			}
		}
	}
}