package com.han.network;

import java.io.StringReader;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;

import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;
import com.deftsoft.beer.GlobalData;

import android.util.Log;
import android.util.Xml;

public class BeerParser extends Parser {
	public static Integer parseTable02(String strResponse) {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setInput(new StringReader(strResponse));
			
			int eventType = parser.getEventType();
			String xmlNodeName = null;
			String strText = null;
			RecordTbl02 recordTbl02 = null;
			boolean isParseRecordTbl02 = false;
			ArrayList<RecordTbl02> arrRecord = new ArrayList<RecordTbl02>();
			
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) {
					case XmlPullParser.START_TAG:
						xmlNodeName = parser.getName();
						if (xmlNodeName.equals("record")) {
							recordTbl02 = new RecordTbl02();
							isParseRecordTbl02 = true;
						}
						break;
					case XmlPullParser.TEXT:
						strText = parser.getText();
						break;
					case XmlPullParser.END_TAG:
						xmlNodeName = parser.getName();
						if (xmlNodeName.equals("record")) {
							arrRecord.add(recordTbl02);
							isParseRecordTbl02 = false;
						} else {
							if (isParseRecordTbl02) {
								recordTbl02.mapField.put(xmlNodeName, strText);
								recordTbl02.arrKey.add(xmlNodeName);
							}
						}
						break;
					default:
						break;
				}
				eventType = parser.next();
			}
			GlobalData.arrRecordTbl02 = arrRecord;
			Log.e("Record count", Integer.toString(arrRecord.size()));
			return SUCCESS;
		} catch (Exception e){
			e.printStackTrace();
			return FAIL;
		}
	}
	
	public static Integer parseTable01(String strResponse) {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setInput(new StringReader(strResponse));
			
			int eventType = parser.getEventType();
			String xmlNodeName = null;
			String strText = null;
			RecordTbl01 recordTbl01 = null;
			boolean isParseRecordTbl01 = false;
			boolean isDocTypeHtml = false;
//			ArrayList<RecordTbl01> arrRecord = new ArrayList<RecordTbl01>();
			GlobalData.arrReturnRecordTbl01 = new ArrayList<RecordTbl01>();

			while (eventType != XmlPullParser.END_DOCUMENT && !isDocTypeHtml) {
				switch (eventType) {
				
					case XmlPullParser.START_TAG:
						xmlNodeName = parser.getName();
						if (xmlNodeName.equals("html")) {
							isDocTypeHtml = true;
						}
						if (xmlNodeName.equals("record")) {
							recordTbl01 = new RecordTbl01();
							isParseRecordTbl01 = true;
						}
						break;
					case XmlPullParser.TEXT:
						strText = parser.getText();
						break;
					case XmlPullParser.END_TAG:
						xmlNodeName = parser.getName();
						if (xmlNodeName.equals("record")) {
							GlobalData.arrReturnRecordTbl01.add(recordTbl01);
							GlobalData.arrRecordTbl01.add(recordTbl01);
							
							isParseRecordTbl01 = false;
						} else {
							if (isParseRecordTbl01) {
//								Log.e(xmlNodeName, strText);
								recordTbl01.mapField.put(xmlNodeName, strText);
								recordTbl01.arrKey.add(xmlNodeName);
							}
						}
						if (xmlNodeName.equals("total")) {
							GlobalData.COUNT_RECORD_TABLE01 = Integer.parseInt(strText);
							Log.e("total count", Integer.toString(GlobalData.COUNT_RECORD_TABLE01));
						}
						break;
					default:
						break;
				}
				eventType = parser.next();
			}

			Log.e("Record count", Integer.toString(GlobalData.arrReturnRecordTbl01.size()));
			if (isDocTypeHtml) {
				Log.e("isDocTypeHtml", "YES");
				return FAIL;
			}
			return SUCCESS;
			
		} catch (Exception e){
			e.printStackTrace();
			return FAIL;
		}
	}
}
