package com.han.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.deftsoft.beer.GlobalData;

import android.util.Base64;
import android.util.Log;

public class WebService {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	public static final int AUTHORIZATION_FAIL = -2;
	
    private static InputStream is = null;

	public static String callHttpRequestGeneral(String url, String method, List<NameValuePair> params) {
        String strResponse = null;
        try {
            if (method == "POST") {

                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Authorization", getB64Auth(GlobalData.currentUserName, GlobalData.currentPassword));
                httpPost.setEntity(new UrlEncodedFormEntity(params));

                HttpResponse httpResponse = httpClient.execute(httpPost);
    			strResponse = EntityUtils.toString(httpResponse.getEntity());
            } else if (method == "GET") {
            	
            	HttpClient httpClient = GetClient();
                HttpGet httpGet = new HttpGet(url);
                httpGet.setHeader("Authorization", getB64Auth(GlobalData.currentUserName, GlobalData.currentPassword));

                HttpResponse httpResponse = httpClient.execute(httpGet);
    			strResponse = EntityUtils.toString(httpResponse.getEntity());
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        
        Log.e("response", strResponse);

        return strResponse;
    }
	
	public static String callHttpRequestMultiPart(String url, MultipartEntity nameValuePairs) {
		String strResponse = null;
		
        try {
	        HttpClient httpClient = new DefaultHttpClient();
	
	        HttpPost httpPost = new HttpPost(url);
	        httpPost.setEntity(nameValuePairs);
	
	        HttpResponse httpResponse = httpClient.execute(httpPost);
	        strResponse = EntityUtils.toString(httpResponse.getEntity());
			
	        Log.e("response", strResponse);
        } catch (Exception e) {
        	e.printStackTrace();
        	return null;
        }
        
        return strResponse;
	}
	
	public static DefaultHttpClient GetClient() {
		DefaultHttpClient ret = null;

		// sets up parameters
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "utf-8");
		params.setBooleanParameter("http.protocol.expect-continue", false);

		// registers schemes for both http and https
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		registry.register(new Scheme("https", new EasySSLSocketFactory(), 443));
		ThreadSafeClientConnManager manager = new ThreadSafeClientConnManager(
				params, registry);
		ret = new DefaultHttpClient(manager, params);
		return ret;
	}
	
	private static String getB64Auth(String login, String pass) {
		String source = login + ":" + pass;
		String ret = "Basic "
				+ Base64.encodeToString(source.getBytes(), Base64.URL_SAFE
						| Base64.NO_WRAP);
		return ret;
	}
}
