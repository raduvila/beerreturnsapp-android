package com.han.network;

import org.json.JSONObject;

import android.util.Log;

public class Parser {
	
	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public static Integer parseOnlyStatus(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			String strState = jsonObj.getString("state");
			if (!strState.equals("success")) {
				return FAIL;
			}
            
			return SUCCESS;
		} catch (Exception e) {
			
			Log.e("json parsing error", e.getMessage());
			return FAIL;
		}
	}
}
