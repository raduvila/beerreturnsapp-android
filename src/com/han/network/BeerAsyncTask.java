package com.han.network;

import java.util.ArrayList;
import java.util.Date;

import com.beer.database.DatabaseHelper;
import com.beer.model.RecordTbl01;
import com.beer.model.RecordTbl02;
import com.deftsoft.beer.GlobalData;
import com.deftsoft.beer.HomeActivity;
import com.deftsoft.beer.Synchronization;
import com.han.utility.TimeUtility;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class BeerAsyncTask extends AsyncTask<String, Integer, Integer> {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	public static final int AUTHORIZATION_FAIL = -2;

	public String strSync = "";
	
	public Context parent;
	public ProgressDialog dlgLoading;
	
	public String curAction;
	public int nResult;
	
	public static String ACTION_SYNC = "action_sync";
	
	public BeerAsyncTask(Context _parent) {
		parent = _parent;
	}
	
	private Integer readTable01() {
		int result = FAIL;
		
		int start = 1;
		int count = GlobalData.COUNT_RECORD_READ_TABLE01_ONCE;
		boolean isLast = false;
		
		try {
			while(!isLast) {
				if (start > GlobalData.COUNT_RECORD_TABLE01) {
					isLast = true;
					result = SUCCESS;
					break;
				}
				result = BeerWebService.readTable01(Integer.toString(start), Integer.toString(count));
				if (result != SUCCESS) {
					isLast = true;
					break;
				}
				
				Synchronization synchronizationActivity = (Synchronization) parent;
				Thread thread1 = new Thread() {
				    @Override
				    public void run() {
						Synchronization synchronizationActivity = (Synchronization) parent;
						synchronizationActivity.successReadTable1();
					}
				};
				synchronizationActivity.runOnUiThread(thread1);
				
				start = start + count;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return FAIL;
		}

		return result;
	}
	
	protected Integer doInBackground(String... params) {
		nResult = FAIL;
		
		curAction = params[0];
		
		if (curAction.equals(ACTION_SYNC)) {
			try {
				nResult = readTable01();
				
//				nResult = BeerWebService.readTable01(strStart, strCount);
				
				if (nResult == SUCCESS) {
					
					Synchronization synchronizationActivity = (Synchronization) parent;
					Thread threadReadAllTable1 = new Thread() {
					    @Override
					    public void run() {
							Synchronization synchronizationActivity = (Synchronization) parent;
							synchronizationActivity.successReadAllTable1();
						}
					};
					synchronizationActivity.runOnUiThread(threadReadAllTable1);
					
					String curTime = TimeUtility.getCurrentTimeAsFormat(GlobalData.STRING_TIME_FORMAT_SYNC03);
					Log.e("curTime", curTime);
					DatabaseHelper dbHelper = new DatabaseHelper(synchronizationActivity);
					
					ArrayList<RecordTbl02> arrRecordUpload = new ArrayList<RecordTbl02>();
					for (RecordTbl02 record : GlobalData.arrRecordTbl02) {
						String strMODO03 = record.mapField.get("MODO03");
						String strSYNC03 = record.mapField.get("SYNC03");
						
						Date dateMODO03 = TimeUtility.getDateFromString(strMODO03, GlobalData.STRING_TIME_FORMAT_MODO03);
						Date dateSYNC03 = TimeUtility.getDateFromString(strSYNC03, GlobalData.STRING_TIME_FORMAT_SYNC03);
						
						if (dateSYNC03 == null) {
							
							ContentValues updateValues = new ContentValues();
							updateValues.put("SYNC03", curTime);
							dbHelper.updateRecord2(record.id, updateValues);
							
							record.mapField.put("SYNC03", curTime);
							arrRecordUpload.add(record);
//							nResult = BeerWebService.uploadTable02(record);
//							if (nResult == SUCCESS)	continue;
//							
//							break;
						} else {
							if (dateMODO03.after(dateSYNC03)) {
								ContentValues updateValues = new ContentValues();
								updateValues.put("SYNC03", curTime);
								dbHelper.updateRecord2(record.id, updateValues);
								
								record.mapField.put("SYNC03", curTime);
								arrRecordUpload.add(record);
							}
						}
						GlobalData.arrRecordTbl02 = dbHelper.getAllRecord2();
					}
					
					Thread thread2 = new Thread() {
					    @Override
					    public void run() {
							Synchronization synchronizationActivity = (Synchronization) parent;
							synchronizationActivity.addLog(strSync);
						}
					};
					
					strSync = "Uploading returns";
					synchronizationActivity.runOnUiThread(thread2);
					
					int k = 0;
					for (RecordTbl02 record : arrRecordUpload) {
						k++;
						nResult = BeerWebService.uploadTable02(record);
						if (nResult == SUCCESS)	{
							strSync = "Uploading return " + Integer.toString(k) + " of " + Integer.toString(arrRecordUpload.size());
							synchronizationActivity.runOnUiThread(thread2);
							continue;
						} else {
							break;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				return FAIL;
			}
		}
		
		return nResult;
	}
	
	protected void onPreExecute() {
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\tLoading...");
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}
	
	protected void onPostExecute(Integer result) {
		if (dlgLoading.isShowing())
			dlgLoading.dismiss();
		
		if (curAction.equals(ACTION_SYNC)) {
			if (nResult == SUCCESS) {
				Synchronization synchronizationActivity = (Synchronization) parent;
				synchronizationActivity.addLog("All Returns Uploaded Successfully");
				synchronizationActivity.suceessUploadTable2();
			}
			if (nResult == FAIL) {
				Synchronization synchronizationActivity = (Synchronization) parent;
				synchronizationActivity.addLog("Failed to contact Server, check configuration");
			}
			if (nResult == AUTHORIZATION_FAIL) {
				Synchronization synchronizationActivity = (Synchronization) parent;
				synchronizationActivity.addLog("Authentication failed");
			}
		}
	}
	
	protected void onProgressUpdate(Integer... progress) {
		
	}
}
