package com.han.network;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.beer.model.RecordTbl02;
import com.deftsoft.beer.GlobalData;

import android.util.Log;

public class BeerWebService extends WebService {

	public static String URL_READ_TABLE02 = "http://82.69.20.215:6400/data/test/table02/";
	public static String URL_READ_TABLE01 = "http://82.69.20.215:6400/data/test/table01/";
	
	public static Integer readTable02(String strStart, String strCount) {
		
		String url = GlobalData.currentServerUrl + "/table02/";
		if (strStart != "") {
			url += ("?start=" + strStart);
		}
		if (strCount != "") {
			url += ("&count=" + strCount);
		}
		Log.e("read table02 url", url);
		Log.e("method", "read table 02");
		
		String strResponse = WebService.callHttpRequestGeneral(url, "GET", null);
		if (strResponse == null) return FAIL;
		
		return BeerParser.parseTable02(strResponse);
	}
	
	public static Integer readTable01(String strStart, String strCount) {
		
		String url = GlobalData.currentServerUrl + "/table01/";
		if (strStart != "") {
			url += ("?start=" + strStart);
		}
		if (strCount != "") {
			url += ("&count=" + strCount);
		}
		
		Log.e("read table01 url", url);
		Log.e("method", "read table 01");
		
		String strResponse = WebService.callHttpRequestGeneral(url, "GET", null);
		if (strResponse == null) return FAIL;
		if (strResponse.equals("")) return AUTHORIZATION_FAIL;
		
		return BeerParser.parseTable01(strResponse);
	}
	
	public static Integer uploadTable02(RecordTbl02 record) {
		
		String url = GlobalData.currentServerUrl + "/table02/";
				
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		
		String strCLNO03 = record.mapField.get("CLNO03"); if (strCLNO03 == null) strCLNO03 = "";
		String strCNNO03 = record.mapField.get("CNNO03"); if (strCNNO03 == null) strCNNO03 = "";
		String strSTSO03 = record.mapField.get("STSO03"); if (strSTSO03 == null) strSTSO03 = "";
		String strDIPV03 = record.mapField.get("DIPV03"); if (strDIPV03 == null) strDIPV03 = "";
		String strCTTO03 = record.mapField.get("CTTO03"); if (strCTTO03 == null) strCTTO03 = "";
		String strCFCO03 = record.mapField.get("CFCO03"); if (strCFCO03 == null) strCFCO03 = "";
		String strMODO03 = record.mapField.get("MODO03"); if (strMODO03 == null) strMODO03 = "";
		String strSYNC03 = record.mapField.get("SYNC03"); if (strSYNC03 == null) strSYNC03 = "";
		
		params.add(new BasicNameValuePair("CLNO03", strCLNO03));
		params.add(new BasicNameValuePair("CNNO03", strCNNO03));
		params.add(new BasicNameValuePair("STSO03", strSTSO03));
		params.add(new BasicNameValuePair("DIPV03", strDIPV03));
		params.add(new BasicNameValuePair("CTTO03", strCTTO03));
		params.add(new BasicNameValuePair("CFCO03", strCFCO03));
		params.add(new BasicNameValuePair("MODO03", strMODO03));
		params.add(new BasicNameValuePair("SYNC03", strSYNC03));

		Log.e("method", "upload table02");
        String strResponse = WebService.callHttpRequestGeneral(url, "POST", params);
        
        return SUCCESS;
	}
}
