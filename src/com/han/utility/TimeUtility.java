package com.han.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

public class TimeUtility {
	public static String getCurrentTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = df.format(Calendar.getInstance().getTime());
		
		Log.e("userLastLogin", date);
		return date;
	}
	
	public static String getCurrentTimeAsFormat(String strFormat) {
		DateFormat df = new SimpleDateFormat(strFormat);
		String time = df.format(Calendar.getInstance().getTime());
		
		return time;
	}
	
	public static Date getDateFromString(String strDate, String strFormat) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat(strFormat); // here set the pattern as you date in string was containing like date/month/year
			Date d = sdf.parse(strDate);
			return d;
		} catch(Exception ex){
			    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
			Log.e("error", ex.toString());
			return null;
		}
	}
	
	public static String getDateAfter(int k) {
		
		Calendar nowCalendar = Calendar.getInstance(); 		
//		Log.e("dayOfWeek", Integer.toString(dayOfWeek));
		
		long nowTime = nowCalendar.getTimeInMillis();
		long afterTime = nowTime + 86400000 * k;
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date afterDate = new Date(afterTime);
		String strDate = df.format(afterDate);
		
		return strDate;	
	}
}
